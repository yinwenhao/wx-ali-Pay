package com.ruoyi.common.utils.pay;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;

import java.io.FileWriter;
import java.io.IOException;


public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String APPID = "2021002123687057";
    // 应用私钥(自己保存)
    public static String PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCK8Y8z83D1ryyfAwgrJqUsQQAu4HHMzdx9zTblx2mBhmS8IS2OQTCaYzuGkrlVZFs7Na0kwV3OfIJ4dWgTRCl+wj3DY0jz4n9EalZmLcSSYxHkHcAJcttGLYG14fgnco7vq1IWlNjqa5d2OR/WryEGdmDkWlNnijv2Jbwe7dBYLFTJcW/St4vdJDXOVnwvz1bRvN2qnewXB9t5XQqafLVqPsOk1G2Jtqk88jLYKN4Bbt8fm2hFlpRtX0w0hgKK+XlBapKFrdCDQfKYn8k+LATyDXSOzrnf7Z5ncs2Qy/N3coQBQPow6bzPm7ztth5xDbh/sXQ4t/CbGunCH0+9PWQpAgMBAAECggEAbQb0Gg3zG7ajyvr0E/g1MC6pWB9i7RS06jp304LQ9YRh04YtWy0oWF15vjWwHKOtOx75iJf7ZiYpsyBd1JBuBzugf4uPGWEoAKV4uqE7WKtIxLnbkQ6BpeXbnL9+nGW2Z7cFqRaPyBM5PNnCdBHkVk/Z5BZE0ECWoKYnohV2oViu8eq1uQ1XOLUmDeArSb5DLUjPcDEjjHO54EoQow95efSZ629/vnJq6j2V4e8h39rXb6E+Kn5O4iF90fqdbx1DdnnTL8uhDBAayvn1Q4czFPdHbRVR8iDu5KLBE70ieNAlgYC17OdFeaOIH1H4f5BaZKfm56ptujzXe9vFyDNAAQKBgQDNjsOJC8Zz1P92ousZ/91YtijXcMiElhlktTQ7fpHezOsi9ZZrKQX8IBzTZDi3ufEwFJqUVZw40bbhx6GUR6tgat4DqQiL44YjNdeSArw2mUGLSLmjxWmS+L0eL212W/dY6Lg9JjaRMqSGwjq1/ejX6KhbwuUmcpgovZnKa+qQKQKBgQCtChDL93ZYJgefGyPkroTR5SiTMtn+zs0ox8ihAcWt/yeK+uvL2UHUJaRg5h53ToGjzN03Ue7myp5j5ZZeQ88xvU3WK0ZA1Q1yCuBZGGOd5l6r5JQ638A0OXyE02J//CLqkojXQgyxfNepfJBOgbIQdq/U7jSSM7BGQYvBSwa0AQKBgQCyatwls+bI23PxUW+m3YMOYO4KMYaaLn5T/0Qa771FGbBuY6Pv12euc2ERysfAENDb3sZdQgGa/8wxJMmGnr6NrbSOkMUuRopDPZSi02TyLwvJDZK0J8F/JA9ih+AG/8Qtk7F2whhS5xYPMb8r//t+jB61pvCm8ohHtGosXIb4IQKBgHYkPi2UKieq5184/3udr69C3z4I5Q90kFw8cv2CoT3XzF62TJFkJu66OI2z768TWa3FLHAdr5fBVPztfomjkG3sSp1j9FZFYCWxyVTrMvBWneKMcKv1olvFt/9T95dcRVaUb3tQbCwuCWx9UGVZZYKaFhix/hCn271L+SgimlQBAoGBAKLRgZkwcpCtERWDWOiCJp4UdAX/OCVh4d1HifaANfV1EpcQ0Ie6pBrD3iIWTcr1iTaOzpvr3fyFPbSqmwr5s1IgipBJ8CYM2O+oydN7MEZQsX+6wubgVEe+C5G4d3hb0ABXRepMik1XZwmPG0QDOkGcKF2fiXpputNw4dqa6qpR";

    // 对应APPID下的支付宝公钥
    public static String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnDhCH9QppzRw+ZArPOLdfEGMotTiGnj/ZnMObb2+LBTtZXTXiwcgmgDfdIX85Sgkw8MoDX8aE8QcMWZGeRnXKFYrI3rdzkQERoR61nKaDtOGyB83e6uPV6V6TM7h4hE/YAJPS/vpUkx3Jobk/9AUlvF2T99JSielaIpNj2N4n7GLeWoRjZq7m9hhsE1UlxxsvgI/HtthBQZGZTLGZTfPT/+me7NTLO/PRq9gR06upy19vqJs35eyH20FOWIohv2dswnvW+w2uFWCICK/YDz5hlWSBpFFxwYp5O+CimLR8oQ6a/E2kfW8AS5T0EmUDXy7he0K127Xle5XycjWua1lQQIDAQAB";

    public static String PID = "2088041505876033";

    // 签名方式
    public static String SIGN_TYPE = "RSA2";
    public static String FORMAT = "json";//参数格式为json

    // 字符编码格式
    public static String CHARSET = "utf-8";

    // 支付宝网关
    public static String GATEWAY_URL = "https://openapi.alipay.com/gateway.do";

    // 支付宝网关
    public static String log_path = "D:\\alipay\\";

    /**
     * 统一收单交易创建接口
     */
    private static AlipayClient alipayClient = null;


    /**获得初始化的AlipayClient
     * @return 支付宝客户端
     */

    public  static AlipayClient getAlipayClient() {
        if (alipayClient == null) {
            synchronized (AlipayConfig.class) {
                if (null == alipayClient) {
                    alipayClient = new DefaultAlipayClient(GATEWAY_URL, APPID, PRIVATE_KEY, FORMAT, CHARSET, PUBLIC_KEY, SIGN_TYPE);
                }
            }
        }
        return alipayClient;
    }



//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

