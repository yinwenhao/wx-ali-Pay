package com.ruoyi.framework.config;


import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.ruoyi.common.constant.ApiConstants;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Binary Wang
 */
@Configuration
@ConditionalOnClass(WxPayService.class)
public class WxPayConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public WxPayService wxService() {
        WxPayConfig payConfig = new WxPayConfig();
        payConfig.setAppId(ApiConstants.APPLICATION_ID);  //appid
        payConfig.setMchId(ApiConstants.MCH_ID);  //微信支付商户号
        payConfig.setMchKey(ApiConstants.PAYKEY);  //微信支付商户密钥
        payConfig.setSubAppId("");  //服务商模式下的子商户公众账号ID，普通模式请不要配置，请在配置文件中将对应项删除
        payConfig.setSubMchId("");  //服务商模式下的子商户号，普通模式请不要配置，最好是请在配置文件中将对应项删除
        payConfig.setKeyPath("");   //apiclient_cert.p12文件的绝对路径，或者如果放在项目中，请以classpath:开头指定
        payConfig.setSignType("MD5");
        WxPayService wxPayService = new WxPayServiceImpl();
        wxPayService.setConfig(payConfig);
        return wxPayService;
    }

}
