package com.ruoyi.common.utils.pay;

import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.ruoyi.common.constant.ApiConstants;

import java.math.BigDecimal;

/**
 * @author ywh
 * @date 2021/5/24
 * @desc
 */
public class AliPayUtils {



    public static AlipayTradeAppPayRequest toAppPay(BigDecimal orderPrice, String payNo, Long orderId)  {
        //1、获得初始化的AlipayClient
        AlipayClient client= AlipayConfig.getAlipayClient();
        //2、设置请求参数
        AlipayTradeAppPayRequest alipayRequest = new AlipayTradeAppPayRequest();
        //页面跳转同步通知页面路径
        alipayRequest.setReturnUrl("");
        // 服务器异步通知回调逻辑
        alipayRequest.setNotifyUrl(ApiConstants.ALI_NOTIFY_URL);
        //封装参数
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setOutTradeNo(payNo);
        model.setSubject("课程购买");  // 订单标题
        model.setTotalAmount(orderPrice.toString());
        model.setProductCode("QUICK_MSECURITY_PAY");//销售码
        model.setTimeoutExpress("30m");
        // 卖家支付宝账号ID，用于支持一个签约账号下支持打款到不同的收款账号，(打款到sellerId对应的支付宝账号) 如果该字段为空，则默认为与支付宝签约的商户的PID，也就是appid对应的PID
        //model.setSellerId(AlipayConfig.PID);
        alipayRequest.setBizModel(model);
        return alipayRequest;
    }
}
